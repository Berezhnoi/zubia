'use strict';

module.exports = {
    'zubia-auth': require('zubia-auth'),
    config      : require('./plugins/config'),
    healthy     : require('./plugins/healthy'),
    user        : require('./plugins/user')
};
