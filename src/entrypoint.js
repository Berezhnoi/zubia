'use strict';

const Glue     = require('glue');
const config   = require('config');

const options = {
    relativeTo: __dirname
};

const pluginList = require('./plugin-list');

const manifest = {
    server       : config.get('server').server,
    connections  : config.get('server').connections,
    registrations: []
};

/* added only for heroku */
manifest.connections[0].port = process.env.PORT || manifest.connections[0].port;

for (let pluginName in pluginList) {
    if (pluginList.hasOwnProperty(pluginName)) {
        let pluginConfig = config.get('plugins')[pluginName];

        manifest.registrations.push({
            plugin : {
                register: pluginList[pluginName].registerPath,
                options : pluginConfig.options
            },
            options: pluginConfig.setup
        });
    }
}

Glue.compose(manifest, options, (err, server) => {
    if (err) {
        throw err;
    }

    server.start(() => {
        console.log('API started at ' + server.info.uri);
    });
});
