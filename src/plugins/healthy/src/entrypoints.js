'use strict';

module.exports = [
    {
        method : 'GET',
        path   : '/',
        handler: require('./entrypoints/get'),
        config : {}
    }
];
