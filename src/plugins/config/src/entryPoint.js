'use strict';

const DsHandler = require('xo-ds-handler');
const dsHandler = new DsHandler();

module.exports = function(options, next) {
    
    dsHandler.config(options.get('ds-handler'))
      .then(next);
};
