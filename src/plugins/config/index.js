'use strict';

const entryPoint    = require('./src/entryPoint');

exports.registerPath = __dirname;

exports.register = (server, options, next) => {
    entryPoint(options, next);
};

exports.register.attributes = {
    name    : __dirname.substr(__dirname.lastIndexOf('/') + 1),
    multiple: false
};
