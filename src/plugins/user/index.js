'use strict';

const entryPoints = require('./src/entrypoints');

exports.registerPath = __dirname;

exports.register = (server, options, next) => {
    server.bind({
        options
    });

    entryPoints.forEach(function(entryPoint) {
        server.route(entryPoint);
    });

    next();
};

exports.register.attributes = {
    name    : __dirname.substr(__dirname.lastIndexOf('/') + 1),
    multiple: false
};
