const frisby = require('frisby');

const userId = '55650bd7-f8fa-4206-9d2c-c4e9d5f88deb';

frisby.create('Request an user with a valid id should be succesful and return proper body')
    .get('http://localhost:8000/user/' + userId)
    .expectStatus(200)
    .expectBodyContains("screenName")
    .expectBodyContains("email")
    .toss();

const badUserId = userId + 'BAD';

frisby.create('Request an user with a invalid id token should not be succesful')
    .get('http://localhost:8000/user/' + badUserId)
    .expectStatus(400)
    .toss();
