/* Nothing here yet */

const frisby = require('frisby');

const userId = '55650bd7-f8fa-4206-9d2c-c4e9d5f88deb';

frisby.create('Sending an image link should update it in the database')
    .put('http://localhost:8000/user/' + userId, {
        avatar: 'https://github.com/'
    })
    .expectStatus(200)
    .toss();

// This doesn't work. Why??
// frisby.create('Sending an array of topics should update it in the database')
//     .put('http://localhost:8000/user/' + userId, {
//         topics: ['https://github.com/', 'https://google.com/', 'https://facebook.com/']
//     })
//     .expectStatus(200)
//     .toss();