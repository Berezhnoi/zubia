/**
 * Created by RaulTomesc on 09/06/16.
 */
'use strict';

const Hoek = require('hoek');

const userMapper = require('./user');

module.exports = {
    output: function(data, broadcast, showEmail) {
        return {
            user: userMapper.output(data, showEmail),
            broadcast: broadcast
        }
    }
};