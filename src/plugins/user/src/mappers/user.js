'use strict';

const Hoek = require('hoek');

module.exports = {
    output: function(data, showEmail) {
        var result = {
            id        : data.id,
            fbId      : data.fbId || '',
            userType  : data.userType || 'zubia user',
            screenName: data.screenName || '',
            broadcasts: {
                scheduled    : data.broadcasts ? data.broadcasts.scheduled || [] : [],
                subscriptions: data.broadcasts ? data.broadcasts.subscriptions || [] : []
            },
            avatar    : data.avatar || '',
            topics    : data.topics || [],
            timezone  : data.timezone || '',
            timestamp : data.timestamp,
            rating    : data.rating ? data.rating.value : 0,
            count: {
                ratings: data.rating ? data.rating.weight : 0,
                viewers: data.viewers || 0
            },
            bio       : data.bio || '',
            updates   : data.updates || [],
            following : data.following || [],
            followers : data.followers || []
        };

        if(showEmail) {
            result.email = data.email
        }

        return result
    }
};