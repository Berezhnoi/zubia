'use strict';

const Boom   = require('boom');
const Hoek   = require('hoek');

const buildOutput = (message, data) => {
    const badRequest = Boom.badRequest(message);

    badRequest.output.payload = Hoek.merge(badRequest.output.payload, data);

    return badRequest;
};

module.exports = function(err) {
    if (err.isJoi) {
        let fields = {};

        err.details.forEach(fieldErr => {
            fields[fieldErr.path] = fieldErr.type;
        });

        return buildOutput('validation error', {
            type    : 'fields',
            fields  : fields
        });
    } else if (err.isZubiaError) {
        return buildOutput(err.message, err.extra);
    } else {
        return buildOutput(err.message, {
            type    : 'system',
            stack   : err.stack
        });
    }
};