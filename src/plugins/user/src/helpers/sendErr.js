'use strict';

const Boom = require('../helpers/boomWrapper');
const _    = require('lodash');

module.exports = function(err) {
    const isGeneralError      = () => !err.isJoi;
    const userDoesNotExist    = () => err.userDoesNotExist;
    const userAlreadyExists   = () => err.userAlreadyExists;
    const passwordIsIncorrect = () => err.passwordIsIncorrect;
    const capitalize          = str => str.charAt(0).toUpperCase() + str.slice(1);
    const getProperError      = (error, required, wrong) => {
        if (required) {
            return capitalize(error.path) + ' is required.';
        } else if (wrong) {
            return capitalize(error.path) + ' is incorrect.';
        } else {
            return 'Unkown error.';
        }
    };
    
    if (isGeneralError()) {
        if (userDoesNotExist()) {
            this.reply(Boom.badRequest({
                type   : 'general',
                message: 'User does not exist.'
            }));
        } else if (userAlreadyExists()) {
            this.reply(Boom.badRequest({
                type   : 'general',
                message: 'User already exists.'
            }));
        } else if (passwordIsIncorrect()) {
            this.reply(Boom.badRequest({
                type  : 'fields',
                fields: {
                    password: 'Password is incorrect.'
                }
            }));
        } else {
            this.reply(Boom.badRequest({
                type   : 'general',
                message: 'A general error has occured.'
            }));
        }
    } else {
        let fields = _.uniqBy(err.details, 'path');

        let retVal = {};

        fields.forEach(field => {
            const required     = field.type.indexOf('required') > -1;
            const wrong        = field.type.indexOf('regex') > -1 || field.type.indexOf('empty') > -1 || field.type.indexOf('email');
            retVal[field.path] = getProperError(field, required, wrong);
        });

        this.reply(Boom.badRequest({
            type  : 'fields',
            fields: retVal
        }));
    }
};