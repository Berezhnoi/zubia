/**
 * Created by gabrielpoamaneagra on 25/07/16.
 */
'use strict';

const promise   = require('bluebird');
const apn 		= require('apn');
const format    = require('date-format');

const broadcastReschedule = function() {
    const broadcast = this.notification.payload;
    const timestamp = format.asString('MM/dd/yy hh:mm', new Date(broadcast.scheduled.planned));

    this.note.alert = `Broadcast ${broadcast.title} has been rescheduled to ${timestamp}`;
};

const broadcastDelete = function() {
    const broadcast = this.notification.payload;

    this.note.alert = `Broadcast ${broadcast.title} has been canceled`;
};

const broadcastCreate = function() {
    const broadcast = this.notification.payload;
    const timestamp = format.asString('MM/dd/yy hh:mm', new Date(broadcast.scheduled.planned));

    this.note.alert = `${broadcast.host.screenName} scheduled the broadcast ${broadcast.title} on ${timestamp}`;
};

const prepareNote = function() {
    this.note.expiry = Math.floor(Date.now() / 1000) + 3600 * 3;
    this.note.sound = "ping.aiff";
    this.note.badge = 0;
    this.note.payload = {};
};

const push = function() {
    this.connection.pushNotification(this.note, this.device);
};

module.exports = function(token, notification) {
    var handler = null;

    var context = {
        connection  : new apn.Connection({
            cert: __dirname + '/correct_cert.pem',
            key : __dirname + '/correct_cert.pem'
        }),
        device      : apn.Device(token),
        note        : new apn.Notification(),
        notification: notification
    };

    switch(notification.type) {
        case 'broadcast-reschedule': {
            handler = broadcastReschedule;
            break;
        }
        case 'broadcast-delete': {
            handler = broadcastDelete;
            break;
        }
        case 'broadcast-create': {
            handler = broadcastCreate;
            break;
        }
    }

    if(!handler) {
        return promise.resolve(true);
    }

    return promise.resolve(true)
        .bind(context)
        .then(prepareNote)
        .then(handler)
        .then(push)
};