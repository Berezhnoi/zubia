'use strict';

const util = require('util');
const Hoek = require('hoek');

module.exports = {
    general : function(message, extra) {
        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;
        this.message = message;
        this.extra = Hoek.applyToDefaults({
            type: 'general'
        }, extra || {});
        this.isZubiaError = true;
    },
    fields  : function(message, extra) {
        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;
        this.message = message;
        this.extra = {
            type    : 'fields',
            fields  : extra
        };
        this.isZubiaError = true;
    }
};

util.inherits(module.exports.general, Error);
util.inherits(module.exports.fields, Error);