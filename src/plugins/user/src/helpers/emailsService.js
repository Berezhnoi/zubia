'use strict';

const Handlebars    = require('handlebars');
const AWS           = require('aws-sdk');
const promise       = require('bluebird');
const fs            = require('fs');
const format        = require('date-format');

const broadcastReschedule = function() {
    const broadcast = this.notification.payload;
    const display = {
        current: {
            date: format.asString('MM/dd/yy', new Date(broadcast.scheduled.planned)),
            time: format.asString('hh:mm', new Date((new Date(broadcast.scheduled.planned)).getTime() - 4 * 60 * 60 * 1000)) + ' EST'
        },
        old: {
            date: format.asString('MM/dd/yy', new Date(broadcast.old.scheduled.planned)),
            time: format.asString('hh:mm', new Date((new Date(broadcast.scheduled.planned)).getTime() - 4 * 60 * 60 * 1000)) + ' EST'
        }
    };

    this.template   = 'broadcast-reschedule';
    this.subject    = `Zubia Broadcast has been Rescheduled`;
    this.info       = {
        broadcast   : broadcast,
        display     : display,
        user        : this.user
    };
};

const broadcastDelete = function() {
    const broadcast = this.notification.payload;
    const display = {
        date: format.asString('MM/dd/yy', new Date(broadcast.scheduled.planned)),
        time: format.asString('hh:mm', new Date((new Date(broadcast.scheduled.planned)).getTime() - 4 * 60 * 60 * 1000)) + ' EST'
    };

    this.template   = 'broadcast-delete';
    this.subject    = `Zubia Broadcast has been Cancelled`;
    this.info       = {
        broadcast   : broadcast,
        display     : display,
        user        : this.user
    };
};

const broadcastCreate = function() {
    const broadcast = this.notification.payload;
    const display = {
        current: {
            date: format.asString('MM/dd/yy', new Date(broadcast.scheduled.planned)),
            time: format.asString('hh:mm', new Date((new Date(broadcast.scheduled.planned)).getTime() - 4 * 60 * 60 * 1000)) + ' EST'
        }
    };

    this.template   = 'broadcast-create';
    this.subject    = `New Zubia Broadcast from ${broadcast.host.screenName}`;
    this.info       = {
        broadcast   : broadcast,
        display     : display,
        user        : this.user
    };
};

const sendEmail = function() {
    const ses   = new AWS.SES({
        apiVersion: '2010-12-01'
    });
    const to    = [].concat(this.to);
    const from  = this.from;
    const tpl   = Handlebars.compile(fs.readFileSync(__dirname + `/../emails/${this.template}.email.html`, 'utf8'));
    const html  = tpl(this.info);

    var email = {
        Source      : from,
        Destination : {
            ToAddresses: to
        },
        Message     : {
            Subject : {
                Data: this.subject
            },
            Body    : {
                Html: {
                    Data    : html,
                    Charset : 'UTF-8'
                }
            }
        }
    };

    return new Promise(function(resolve, reject) {
        ses.sendEmail(email, function(err, data) {
            if (err) {
                return reject(err);
            }

            return resolve(data);
        });
    });
};

module.exports = function(user, notification, plugin) {
    var handler = null;
    AWS.config  = plugin.options.aws;

    var context = {
        user        : user,
        notification: notification,
        from        : plugin.options.email.from,
        to          : user.email,
        subject     : null,
        info        : null,
        template    : null
    };

    switch(notification.type) {
        case 'broadcast-reschedule': {
            handler = broadcastReschedule;
            break;
        }
        case 'broadcast-delete': {
            handler = broadcastDelete;
            break;
        }
        case 'broadcast-create': {
            handler = broadcastCreate;
            break;
        }
    }

    if(!handler) {
        return promise.resolve(true);
    }

    return promise.resolve(true)
        .bind(context)
        .then(handler)
        .then(sendEmail)
};
