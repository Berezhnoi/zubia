'use strict';

const Boom = require('boom');
const Hoek = require('hoek');

exports.badRequest = (message, data) => {
    const badRequest = Boom.badRequest();

    badRequest.output.payload = Hoek.merge(badRequest.output.payload, data);

    return badRequest;
};