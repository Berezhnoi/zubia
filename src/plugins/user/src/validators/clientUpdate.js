'use strict';

const Joi = require('joi');

module.exports = {
    input: Joi.object().keys({
        type        : Joi.string().required(),
        payload     : Joi.any().required()
    })
};