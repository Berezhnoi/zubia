'use strict';

const Joi = require('joi');

module.exports = {
    input: Joi.object().keys({
        emails: Joi.array()
    })
};
