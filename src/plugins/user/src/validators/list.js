'use strict';

const Joi = require('joi');

module.exports = {
    input: Joi.object().keys({
        page        : Joi.number().integer().min(0).required(),
        perPage     : Joi.number().integer().min(1).required(),
        status      : Joi.alternatives().try(Joi.string(), Joi.array()),
        sort        : Joi.object(),
        ids         : Joi.array()
    })
};