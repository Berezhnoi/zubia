'use strict';

const Joi = require('joi');

module.exports = {
    input: Joi.object().keys({
        avatar: Joi.string().allow(''),
        topics: Joi.array().items(Joi.string().guid()),
        screenName: Joi.string().min(3).max(20),
        rating: Joi.object().keys({
            value: Joi.number().integer().required()
        }),
        bio     : Joi.string().allow(''),
        viewers : Joi.number()
    }).or('avatar', 'topics', 'rating', 'bio', 'screenName', 'viewers')
};
