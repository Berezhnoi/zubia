'use strict';

const Joi = require('joi');

module.exports = {
    input: Joi.object().keys({
        broadcastId     : Joi.string().guid(),
        userId: Joi.string().guid()
    })
};
