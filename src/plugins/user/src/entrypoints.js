'use strict';

module.exports = [
    {
        method : 'PUT',
        path   : '/{id}',
        handler: require('./entrypoints/putId'),
        config : {}
    },
    {
        method : 'GET',
        path   : '/{id}',
        handler: require('./entrypoints/getId'),
        config : {
            
        }
    },
    {
        method : 'GET',
        path   : '/facebokUser/{id}',
        handler: require('./entrypoints/getFbUser'),
        config : {
            
        }
    },
    {
        method : 'DELETE',
        path   : '/{id}',
        handler: require('./entrypoints/deleteId'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/subscribe',
        handler: require('./entrypoints/subscribe'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/unsubscribe',
        handler: require('./entrypoints/unsubscribe'),
        config : {
           auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/schedule',
        handler: require('./entrypoints/schedule'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/unschedule',
        handler: require('./entrypoints/unschedule'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/get-users-by-ids',
        handler: require('./entrypoints/getUsersByIds'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/get-users-by-emails',
        handler: require('./entrypoints/getUsersByEmails'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/multiple-subscribe',
        handler: require('./entrypoints/multipleSubscribe'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/{id}/client-update',
        handler: require('./entrypoints/clientUpdate'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'DELETE',
        path   : '/client-update',
        handler: require('./entrypoints/clearClientUpdates'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/list',
        handler: require('./entrypoints/list'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/init',
        handler: require('./entrypoints/ini'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'POST',
        path   : '/following/{id}',
        handler: require('./entrypoints/follow'),
        config : {
            auth: 'zubia-token'
        }
    },
    {
        method : 'DELETE',
        path   : '/following/{id}',
        handler: require('./entrypoints/unfollow'),
        config : {
            auth: 'zubia-token'
        }
    }
];
