'use strict';

const buildErr   = require('../helpers/buildErr');
const ZubiaError = require('../helpers/zubiaError');
const mapper     = require('../mappers/user');
const validators = require('../validators/putId');

const promise = require('bluebird');
const Joi     = require('joi');
const Hoek    = require('hoek');

const validate = promise.promisify(Joi.validate);
let DsHandler, dsHandler;

const validateRawData = function() {
    return validate(this.payload, validators.input, {abortEarly: false});
};

const getUser = function() {
    return dsHandler.read({
        dataSource: 'user.users',
        query     : {
            'data->>id': this.id
        }
    })
};

const setUser = function(data) {
    if (!data[0]) {
        throw new ZubiaError.general('User not found');
    }

    this.user = data[0];
};

const updateRatings = function() {
    if (!this.payload.rating) {
        return;
    }

    if (!this.user.rating) {
        this.payload.rating = {
            value : this.payload.rating.value,
            weight: 1
        }
    } else {
        const userValue  = this.user.rating.value;
        const userWeight = this.user.rating.weight;
        const newValue   = this.payload.rating.value;

        this.payload.rating = {
            value : (userValue * userWeight + newValue) / (userWeight + 1),
            weight: userWeight + 1
        };
    }
};

const updateUser = function() {
    return dsHandler.update({
            dataSource: 'user.users',
            query     : {
                'data->>id': this.id
            },
            payload   : this.payload
        })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return user;
        });
};

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        id     : request.params.id,
        payload: request.payload,
        user   : request.headers['zubia-auth'],
        reply  : reply
    };

    return promise.resolve()
        .bind(context)
        .then(validateRawData)
        .then(getUser)
        .then(setUser)
        .then(updateRatings)
        .then(updateUser)
        .then(function(user) {
            return mapper.output(user, true);
        })
        .catch(buildErr)
        .then(reply);
};