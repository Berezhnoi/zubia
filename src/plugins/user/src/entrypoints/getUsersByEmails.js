'use strict';

const buildErr   = require('../helpers/buildErr');
const ZubiaError = require('../helpers/zubiaError');
const mapper     = require('../mappers/user');
const validator  = require('../validators/getUsersByEmails');

const promise = require('bluebird');
const Joi     = require('joi');

const validate = promise.promisify(Joi.validate);

let DsHandler, dsHandler;

const validateRawData = function () {
    return validate(this.payload, validator.input, {abortEarly: false});
};

const getUsers = function () {
    const query = `data->>'email' IN('` + this.payload.emails.join(`','`) + `')`;

    return dsHandler.read({
        dataSource: 'user.users',
        native    : query
    });
};

const buildOutput = function (users) {
    return users.map(user => mapper.output(user, true));
};

module.exports = function (request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    let context = {
        payload: request.payload
    };

    return promise.resolve()
        .bind(context)
        .then(validateRawData)
        .then(getUsers)
        .then(buildOutput)
        .catch(buildErr)
        .then(reply);
};
