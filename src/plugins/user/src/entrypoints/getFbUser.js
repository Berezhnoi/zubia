'use strict';

const buildErr   = require('../helpers/buildErr');
const ZubiaError = require('../helpers/zubiaError');
const mapper     = require('../mappers/user');

const promise = require('bluebird');

let DsHandler, dsHandler;

const getUser = function() {
    return dsHandler.read({
            dataSource: 'user.users',
            query     : {
                'data->>fbId': this.id
            }
        })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return Promise.resolve(user);
        });
};

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    let context = {
        id      : request.params.id,
        user    : request.auth.credentials,
        reply
    };

    return promise.resolve()
        .bind(context)
        .then(getUser)
        .then(function(user) {
            return mapper.output(user, user.id == context.id );
        })
        .catch(buildErr)
        .then(reply);
};