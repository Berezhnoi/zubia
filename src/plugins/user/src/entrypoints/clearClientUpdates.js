'use strict';

const buildErr   = require('../helpers/buildErr');
const ZubiaError = require('../helpers/zubiaError');
const mapper     = require('../mappers/user');
const validators = require('../validators/clientUpdate');

const promise = require('bluebird');

let DsHandler, dsHandler;

const updateUser = function() {
    return dsHandler.update({
            dataSource: 'user.users',
            query     : {
                'data->>id': this.user.id
            },
            payload   : {
                updates: []
            }
        })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return user;
        });
};

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        user  : request.auth.credentials,
    };

    return promise.resolve()
        .bind(context)
        .then(updateUser)
        .then(function(user) {
            return mapper.output(user, true);
        })
        .catch(buildErr)
        .then(reply);
};