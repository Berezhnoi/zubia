'use strict';

const buildErr   = require('../helpers/buildErr');
const ZubiaError = require('../helpers/zubiaError');

const promise = require('bluebird');

let DsHandler, dsHandler;

const PASSWORD = 'BAhxrwzBrcL9qeKvs8cF';

const validatePassword = function() {
    if (this.password !== PASSWORD) {
        throw new ZubiaError.general('Incorrect password');
    }
};

const getUser = function() {
    return dsHandler.read({
        dataSource: 'user.users',
        query     : {
            'data->>id': this.id
        }
    })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return Promise.resolve(user);
        });
};

const deleteUser = function () {
    return dsHandler.remove({
        dataSource: 'user.users',
        query     : {
            'data->>id': this.id
        }
    });
};

const buildOutput = function () {
    return {
        success: true
    }
};

module.exports = function (request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    let context = {
        id      : request.params.id,
        password: request.headers["password"]
    };

    return promise.resolve()
        .bind(context)
        .then(validatePassword)
        .then(getUser)
        .then(deleteUser)
        .then(buildOutput)
        .catch(buildErr)
        .then(reply);
};