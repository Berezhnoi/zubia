'use strict';

const promise       = require('bluebird');
const Joi           = require('joi');
const requestify    = require('requestify');

const validators    = require('../validators/schedule');
const buildErr      = require('../helpers/buildErr');
const ZubiaError    = require('../helpers/zubiaError');
const mapper        = require('../mappers/user');

const validate = promise.promisify(Joi.validate);
let DsHandler, dsHandler;

const validateRawData = function() {
    return validate(this.payload, validators.input);
};

const validateBroadcastId = function() {
    return Promise.resolve(true);
    // THIS IS NOT REALLY NEEDED AT THIS POINT SINCE ONLY BROADCAST API REQUESTS THIS AND ALREADY CHECKS

    return requestify.get(this.plugin.options['broadcast-api'] + this.payload.broadcastId, {
            headers: {
                'zubia-auth'    : this.authorization,
                'zubia-version' : this.appVersion
            }
        })
        .then(function(response) {
            let broadcast = response.getBody();

            if (!broadcast.id) {
                throw new ZubiaError.general('Broadcast not found');
            }
        })
        .catch(function(response) {
            if (response.isZubiaError) {
                throw response;
            } else {
                throw new ZubiaError.general(response.getBody().message);
            }
        });
};

const updateUser = function() {
    const index = this.user.broadcasts.scheduled.indexOf(this.payload.broadcastId);

    this.user.broadcasts.scheduled.splice(index, 1);

    return dsHandler.update({
        dataSource  : 'user.users',
        query       : {
            'data->>id': this.user.id
        },
        payload     : {
            broadcasts: this.user.broadcasts
        }
    }).then(function(data) {
        return data[0];
    });
};

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        plugin          : this,
        payload         : request.payload,
        authorization   : request.headers['zubia-auth'],
        appVersion      : request.headers['zubia-version'],
        user            : request.auth.credentials,
        reply           : reply
    };

    return promise.resolve()
        .bind(context)
        .then(validateRawData)
        .then(validateBroadcastId)
        .then(updateUser)
        .then(function(user) {
            return mapper.output(user, true);
        })
        .catch(buildErr)
        .then(reply);
};