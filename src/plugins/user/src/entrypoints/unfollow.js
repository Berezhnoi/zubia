'use strict';

const buildErr   = require('../helpers/buildErr');
const ZubiaError = require('../helpers/zubiaError');
const mapper     = require('../mappers/user');

const promise = require('bluebird');

let DsHandler, dsHandler;

const getFollowing = function() {
    return dsHandler.read({
        dataSource: 'user.users',
        query     : {
            'data->>id': this.id
        }
    }).then(function(data) {
        if(data.length == 0) {
            throw new ZubiaError.general("User not found")
        }

        return data[0]
    })
};

const updateData = function() {
    let currentIndex = -1;

    if (!this.user.following) {
        this.user.following = []
    }

    if (!this.following.followers) {
        this.following.followers = []
    }

    currentIndex = this.user.following.indexOf(this.id);

    if(currentIndex !== -1) {
        this.user.following.splice(currentIndex, 1)
    }

    currentIndex = this.following.followers.indexOf(this.user.id);

    if(currentIndex !== -1) {
        this.following.followers.splice(currentIndex, 1)
    }
};

const updateFollowing = function() {
    return dsHandler.update({
            dataSource: 'user.users',
            query     : {
                'data->>id': this.following.id
            },
            payload   : {
                followers: this.following.followers
            }
        })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return user;
        });
};


const updateUser = function() {
    return dsHandler.update({
            dataSource: 'user.users',
            query     : {
                'data->>id': this.user.id
            },
            payload   : {
                following: this.user.following
            }
        })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return user;
        });
};

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        id          : request.params.id,
        user        : request.auth.credentials,
        following   : null
    };

    return promise.resolve()
        .bind(context)
        .then(getFollowing)
        .then(function(user) {
            this.following = user
        })
        .then(updateData)
        .then(updateFollowing)
        .then(updateUser)
        .then(function(user) {
            return mapper.output(user, true);
        })
        .catch(buildErr)
        .then(reply);
};