'use strict';

const buildErr = require('../helpers/buildErr');
const ZubiaError  = require('../helpers/zubiaError');
const mapper = require('../mappers/user');

const promise    = require('bluebird');
const requestify = require('requestify');

let DsHandler, dsHandler;

const getUser = function(token) {
    return dsHandler.read({
            dataSource: 'user.users',
            query     : {
                'data->>token': token
            }
        })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return Promise.resolve(mapper.output(user, true));
        });
};

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        topicsUrl  : this.options['channels-api'] + 'topic',
        channelsUrl: this.options['channels-api'] + 'channel'
    };

    // getTopics, getChannels and getUser -- if we got an userId
    const promises = [requestify.get(context.topicsUrl), requestify.get(context.channelsUrl)];
    const token    = request.payload ? request.payload.token : null;

    if (token) {
        promises.push(getUser(token));
    }

    return promise.all(promises)
        .then(results => {
            reply({
                topics  : results[0].getBody(),
                channels: results[1].getBody(),
                user    : results[2] || null
            })
        })
        .catch(err => reply(buildErr(err)));
};