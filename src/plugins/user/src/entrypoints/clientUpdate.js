'use strict';

const buildErr          = require('../helpers/buildErr');
const ZubiaError        = require('../helpers/zubiaError');
const pushNotifications = require('../helpers/notificationsService');
const emailNotifications= require('../helpers/emailsService');
const mapper            = require('../mappers/user');
const validators        = require('../validators/clientUpdate');

const promise = require('bluebird');
const Joi     = require('joi');
const Hoek    = require('hoek');

const validate = promise.promisify(Joi.validate);
let DsHandler, dsHandler;

const validateRawData = function() {
    return validate(this.payload, validators.input, {abortEarly: false});
};

const getUser = function() {
    return dsHandler.read({
            dataSource: 'user.users',
            query     : {
                'data->>id': this.id
            }
        })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return Promise.resolve(user);
        });
};

const addUpdate = function() {
    if(!this.user.updates) {
        this.user.updates = [];
    }

    this.user.updates.push(this.payload);
};

const sendPushNotifications = function() {
    if(this.user.pushNotificationsToken) {
        return pushNotifications(this.user.pushNotificationsToken, this.payload);
    }

    return promise.resolve(true);
};

const sendEmailsNotifications = function() {
    return emailNotifications(this.user, this.payload, this.plugin);
};

const updateUser = function() {
    return dsHandler.update({
            dataSource: 'user.users',
            query     : {
                'data->>id': this.user.id
            },
            payload   : {
                updates: this.user.updates
            }
        })
        .then(data => {
            let user = data[0];

            if (!user) {
                throw new ZubiaError.general('User not found');
            }

            return user;
        });
};

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        id      : request.params.id,
        payload : request.payload,
        user    : null,
        plugin  : this
    };

    return promise.resolve()
        .bind(context)
        .then(validateRawData)
        .then(getUser)
        .then(function(user){
            this.user = user;

            return promise.resolve(true)
        })
        .then(addUpdate)
        .then(sendPushNotifications)
        .then(sendEmailsNotifications)
        .then(updateUser)
        .then(function(user) {
            return mapper.output(user, true);
        })
        .catch(buildErr)
        .then(reply);
};