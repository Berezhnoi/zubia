'use strict';

const promise    = require('bluebird');
const Joi        = require('joi');
const requestify = require('requestify');

const validators = require('../validators/schedule');
const buildErr   = require('../helpers/buildErr');
const ZubiaError = require('../helpers/zubiaError');
const mapper     = require('../mappers/subscribe_unsubscribe');

const validate = promise.promisify(Joi.validate);
let DsHandler, dsHandler;

const validateRawData = function() {
    return validate(this.payload, validators.input, {abortEarly: false});
};

const validateBroadcastId = function() {
    return requestify.get(this.plugin.options['broadcast-api'] + this.payload.broadcastId, {
            headers: {
                'zubia-auth'    : this.authorization,
                'zubia-version' : this.appVersion
            }
        })
        .catch(function(response) {
            throw new ZubiaError.general(response.getBody().message);
        })
        .then((response) => {
            let broadcast = response.getBody();

            if (!broadcast.id) {
                throw new ZubiaError.general('Broadcast not found');
            }

            if (this.user.broadcasts.subscriptions.indexOf(this.payload.broadcastId) == -1) {
                throw new ZubiaError.general('You were not subscribed to this broadcast');
            }

        });
};

const updateUser = function() {
    const index = this.user.broadcasts.subscriptions.indexOf(this.payload.broadcastId);

    if(index != -1) {
        this.user.broadcasts.subscriptions.splice(index, 1);
    }

    return dsHandler.update({
        dataSource: 'user.users',
        query     : {
            'data->>id': this.user.id
        },
        payload   : {
            broadcasts: this.user.broadcasts
        }
    }).then(function(data) {
        return data[0];
    });
};

function updateBroadcast() {
    return requestify.post(this.plugin.options['broadcast-api'] + 'unsubscribe', {
            userId     : this.user.id,
            broadcastId: this.payload.broadcastId
        },
        {
            headers: {
                'zubia-auth'    : this.authorization,
                'zubia-version' : this.appVersion
            }
        })
        .then(function(response) {
            let broadcast = response.getBody();

            if (!broadcast.id) {
                throw new ZubiaError.general('Broadcast not found');
            }

            return broadcast;
        })
        .catch(function(response) {
            if (response.isZubiaError) {
                throw response;
            } else {
                throw new ZubiaError.general(response.getBody().message);
            }
        });
}

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        plugin          : this,
        payload         : request.payload,
        authorization   : request.headers['zubia-auth'],
        appVersion      : request.headers['zubia-version'],
        user            : request.auth.credentials,
        reply           : reply
    };

    return promise.resolve()
        .bind(context)
        .then(validateRawData)
        .then(validateBroadcastId)
        .then(updateUser)
        .then(function(user) {
            this.user = user;
        })
        .then(updateBroadcast)
        .then(function(broadcast) {
            this.broadcast = broadcast;
        })
        .then(function() {
            return mapper.output(this.user, this.broadcast, true);
        })
        .catch(buildErr)
        .then(reply);
};
