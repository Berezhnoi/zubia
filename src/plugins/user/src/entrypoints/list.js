'use strict';

const mappers    = require('../mappers/user');
const validators = require('../validators/list');
const buildErr   = require('../helpers/buildErr');

const Joi     = require('joi');
const promise = require('bluebird');

const validate = promise.promisify(Joi.validate);

let DsHandler, dsHandler;

const validateRawData = function () {
    return validate(this.payload, validators.input);
};

const getRows = function () {
    let query = `1 = 1`;
    let sort  = {
        'data->rating->>value' : 'desc',
        'data->rating->>weight': 'desc'
    };

    if (this.payload.status) {
        let statuses = [].concat(this.payload.status);
        query += ` AND data->>'status' IN('` + statuses.join(`','`) + `')`;
    } else {
        query += ` AND data->>'status' NOT IN ('inactive')`;
    }

    if (this.payload.ids) {
        query += ` AND data->>'id' IN('` + this.payload.ids.join(`','`) + `')`;
    }

    if (this.payload.sort) {
        sort = this.payload.sort
    } else {
        query += ` AND data->'rating'->>'value' IS NOT NULL`
    }

    return dsHandler.read({
        dataSource: 'user.users',
        native    : query,
        sort      : sort,
        limit     : this.payload.perPage,
        offset    : this.payload.perPage * this.payload.page
    });
};

const roundValue = function (val) {
    let decimal    = val - Math.floor(val);
    let integ      = Math.floor(val);
    let roundedVal = 0;

    if (decimal >= 0 && decimal <= 0.24) {
        roundedVal = integ
    } else if (decimal >= 0.25 && decimal <= 0.74) {
        roundedVal = integ + 0.5
    } else if (decimal >= 0.75) {
        roundedVal = integ + 1
    }

    return roundedVal;
};

const cmp = function (a, b) {
    return a.rating.weight > b.rating.weight ? -1 : 1;
};

const orderUsers = function (users) {
    let val             = 5;
    let ind             = 0;
    let arrayOfArrays   = [];
    let shouldIncrement = false;

    users.forEach(function(user) {
        if (!user.rating) {
            user.rating = {
                value: 0,
                weight: 0
            }
        }
    });

    while (val != -0.5) {
        users.forEach(function (user) {
            if (roundValue(user.rating.value) === val) {
                if (arrayOfArrays[ind]) {
                    arrayOfArrays[ind].push(user);
                } else {
                    arrayOfArrays.push([]);
                    arrayOfArrays[ind].push(user);
                }

                shouldIncrement = true;
            }
        });

        if(shouldIncrement) {
            shouldIncrement = false;
            ind += 1;
        }

        val -= 0.5;
    }

    let newUsers = [];

    arrayOfArrays.forEach(function (arr) {
        arr.sort(cmp);
        newUsers = newUsers.concat(arr);
    });

    return newUsers;
};

module.exports = function (request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        plugin        : this,
        payload       : request.payload,
        user          : request.auth.credentials,
        broadcastInput: {},
        broadcast     : null
    };

    return promise.resolve()
        .bind(context)
        .then(validateRawData)
        .then(getRows)
        .then(orderUsers)
        .then(function (users) {
            return users.map(function (user) {
                return mappers.output(user, false);
            });
        })
        .catch(buildErr)
        .then(reply);
};
