'use strict';

const promise    = require('bluebird');
const Joi        = require('joi');
const requestify = require('requestify');

const validators = require('../validators/multipleSubscribe');
const buildErr   = require('../helpers/buildErr');
const ZubiaError = require('../helpers/zubiaError');
const mapper     = require('../mappers/subscribe_unsubscribe');

const validate = promise.promisify(Joi.validate);
let DsHandler, dsHandler;

const validateRawData = function() {
    return validate(this.payload, validators.input, {abortEarly: false});
};

const validateBroadcastId = function() {
    return requestify.get(this.plugin.options['broadcast-api'] + this.payload.broadcastId, {
            headers: {
                'zubia-auth'    : this.authorization,
                'zubia-version' : this.appVersion
            }
        })
        .catch(function(response) {
            throw new ZubiaError.general(response.getBody().message);
        })
        .then((response) => {
            let broadcast = response.getBody();

            if (!broadcast.id) {
                throw new ZubiaError.general('Broadcast not found');
            }
        });
};

// User will be either the logged in user or the one
// with the id sent in the payload
const setUsers = function() {
  const query = `data->>'id' IN('` + this.payload.userIds.join(`','`) + `')`;

  return dsHandler.read({
      dataSource: 'user.users',
      native    : query
  }).then(data => {
      this.users = data;

      return Promise.resolve(this.users);
  });
};

const updateUser = function() {
    let promises = [];

    this.users.forEach((user) => {
      if (user.broadcasts.subscriptions.indexOf(this.payload.broadcastId) === -1) {
          user.broadcasts.subscriptions.push(this.payload.broadcastId);
      }

      promises.push(dsHandler.update({
          dataSource: 'user.users',
          query     : {
            'data->>id': user.id
          },
          payload   : {
              broadcasts: user.broadcasts
          }
      }));
    });


    return promise.all(promises);
};

const updateBroadcast = function() {
    return requestify.post(this.plugin.options['broadcast-api'] + 'multiple-subscribe', {
            userIds     : this.payload.userIds,
            broadcastId : this.payload.broadcastId
        },
        {
            headers: {
                'zubia-auth'    : this.authorization,
                'zubia-version' : this.appVersion
            }
        })
        .then(function(response) {
            let broadcast = response.getBody();

            if (!broadcast.id) {
                throw new ZubiaError.general('Broadcast not found');
            }

            return broadcast;
        })
        .catch(function(response) {
            if (response.isZubiaError) {
                throw response;
            } else {
                throw new ZubiaError.general(response.getBody().message);
            }
        });
};

module.exports = function(request, reply) {
    DsHandler = require('xo-ds-handler');
    dsHandler = new DsHandler();

    const context = {
        plugin          : this,
        payload         : request.payload,
        authorization   : request.headers["zubia-auth"],
        appVersion      : request.headers['zubia-version'],
        user            : request.auth.credentials,
        outputUser      : null,
        reply           : reply,
        users: null
    };

    return promise.resolve()
        .bind(context)
        .then(validateRawData)
        .then(setUsers)
        .then(validateBroadcastId)
        .then(updateUser)
        .then(function(user) {
            this.outputUser = user;
        })
        .then(updateBroadcast)
        .then(function(broadcast) {
            this.broadcast = broadcast;
        })
        .then(function() {
            return {
              success: true
            }
        })
        // .catch(buildErr)
        .then(reply);
};
