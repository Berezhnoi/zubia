NAME=zubia/service-user-api
NAME_SHORT=$$(echo $(NAME) | cut -d '/' -f 2)
VERSION=$$(git rev-parse --short HEAD)
NODE_ENV=dev

install:
	@rm -rf ./node_modules
	npm install

build:
	@docker build -t $(NAME) -f docker/Dockerfile .

run: build
	@docker-compose -f docker/docker-compose.yml up

docker-build:
	docker build -t $(NAME):$(VERSION) -f docker/Dockerfile .
	docker tag $(NAME):$(VERSION) $(NAME):latest

docker-push: docker-build
	docker push $(NAME)

deploy:
	@export CURRENT_BRANCH=$$(git rev-parse --abbrev-ref HEAD) && \
	export BUILD_ENV=qa && \
	export NODE_ENV=qa && \
	git checkout qa && \
	git pull && \
	git merge --no-ff --no-edit $$CURRENT_BRANCH && \
	git push && \
	make docker-push && \
	cd deploy && \
	eb deploy && \
	cd ../ && \
	git checkout $$CURRENT_BRANCH

deploy-prod:
	@export CURRENT_BRANCH=$$(git rev-parse --abbrev-ref HEAD) && \
	export BUILD_ENV=production && \
	export NODE_ENV=production && \
	git checkout production && \
	git pull && \
	git merge --no-ff --no-edit $$CURRENT_BRANCH && \
	git push && \
	make docker-push && \
	cd deploy && \
	eb deploy && \
	cd ../ && \
	git checkout $$CURRENT_BRANCH

.PHONY: install docker-build run docker-push deploy build
